# nota_nep_shared 0.1

## Commands

- **moveToPositions** - Moves units to defined positions. If there are more units than positions, divide units uniformly.

## Disclaimer

**The code is far from perfect. Use at your own risk.**