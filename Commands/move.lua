function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Moves units to a given position.",
		parameterDefs = {
			{ 
				name = "position", -- [Vec3] - where to move
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "units", -- [array of units] - which units should move
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "fight", -- [bool] - whether units should fight on their way
				variableType = "expression",
				componentType = "checkBox",
				defaultValue = "false",
			}
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
	self.unitsInfo = {}
end

function Run(self, unitsAll, parameter)
	local position = parameter.position -- array of Vec3
	local units = parameter.units -- array of units
	local fight = parameter.fight -- boolean

	if (self.unitsInfo == nil) then
		self.unitsInfo = {}
		self.threshold = 0
	end

	-- pick the spring command implementing the move
	local cmdID = CMD.MOVE
	if (fight) then cmdID = CMD.FIGHT end

	-- assign positions to units
	for i,unit in ipairs(units) do
		if self.unitsInfo[unit] == nil then
			self.unitsInfo[unit] = { targetPosition = position, treshold = 0 }
			SpringGiveOrderToUnit(unit, cmdID, self.unitsInfo[unit].targetPosition:AsSpringVector(), {})
		end
	end

	local allSuccess = true

	for i,unit in ipairs(units) do
		local unitInfo = self.unitsInfo[unit]
		local targetPosition = unitInfo["targetPosition"]
		local pointX, pointY, pointZ = SpringGetUnitPosition(unit)
		local unitPosition = Vec3(pointX, pointY, pointZ)
		local distance = unitPosition:Distance(targetPosition)

		if (distance > unitInfo["treshold"]) then
			if (unitInfo["bestDistance"] == nil or unitInfo["bestDistance"] > distance + 50) then
				unitInfo["bestDistance"] = distance
				unitInfo["treshold"] = 0
			else 
				unitInfo["treshold"] = unitInfo["treshold"] + THRESHOLD_STEP
			end

			allSuccess = false
		end
	end

	if (allSuccess == true) then
		return SUCCESS
	else
		return RUNNING
	end
end

function Reset(self)
	ClearState(self)
end
