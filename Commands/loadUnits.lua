function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Load units onto a given transporter",
		parameterDefs = {
			{ 
				name = "units", -- which units to load
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "transporter", -- which transporter to use
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end

local function ClearState(self)
	self.started = false
end

function Run(self, allUnits, parameter)
	local units = parameter.units
	local transporter = parameter.transporter

	if (Spring.ValidUnitID(transporter) == false) then
		return FAILURE
	end

	if (self.started ~= true) then
		self.started = true

		for i,unit in ipairs(units) do
			Spring.GiveOrderToUnit(transporter, CMD.LOAD_UNITS, {unit}, {"shift"});
		end
	end
	
	local allLoaded = true

	for _,unit in ipairs(units) do
		if (Spring.ValidUnitID(unit) == false) then
			return FAILURE
		end

		if Spring.GetUnitTransporter(unit) ~= transporter then
			allLoaded = false
		end
	end

	if allLoaded == true then
		return SUCCESS
	else
		return RUNNING
	end
end


function Reset(self)
	ClearState(self)
end
