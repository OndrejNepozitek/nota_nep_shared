function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Unload unit at a given position.",
		parameterDefs = {
			{ 
				name = "unit", -- which unit to unload
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "transporter", -- which transporter to unload
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "position", -- where to unload unit
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end

local function ClearState(self)
	self.commandGiven = false
end

function Run(self, allUnits, parameter)
	local unit = parameter.unit
	local transporter = parameter.transporter
	local position = parameter.position
	
	local allUnloaded = true

	if self.commandGiven ~= true then
		Spring.GiveOrderToUnit(transporter, CMD.UNLOAD_UNIT, {position.x, position.y, position.z}, {});
		self.commandGiven = true
	end

	if (Spring.ValidUnitID(transporter) == false or Spring.ValidUnitID(unit) == false) then
		return FAILURE
	end

	if Spring.GetUnitTransporter(unit) ~= nil then
		return RUNNING
	end

	return SUCCESS
end

function Reset(self)
	ClearState(self)
end
